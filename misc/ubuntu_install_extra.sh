#/bin/sh

sudo apt-get install texlive texlive-science texlive-generic-extra texlive-latex3


#--- install pylearn2 --------------
sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install libamd2.2.0 libblas3gf
sudo apt-get install libc6 libgcc1 libgfortran3 liblapack3gf libumfpack5.4.0 libstdc++6
sudo apt-get install gfortran libatlas-dev python2.7-dev

sudo apt-get install python-numpy python-scipy ipython

sudo apt-get install python-setuptools git-core

sudo apt-get install libyaml-dev python-yaml
sudo easy_install pip

sudo pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git
sudo pip install git+git://github.com/lisa-lab/pylearn2.git

sudo apt-get install python-matplotlib python-scikits-learn
sudo pip install pandas
#----- end pylearn2 --------------




