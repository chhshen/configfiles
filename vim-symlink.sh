#!/bin/bash

P=`pwd`

fs=`ls -a | grep vimrc`

for f in $fs
do
    [ -f  $HOME/$f    ] &&  unlink $HOME/$f
    ln -s -v "$P"/$f $HOME/$f
done

