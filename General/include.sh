#!/bin/bash


NORMAL=$(tput sgr0)
GREEN=$(tput setaf 2; tput bold)
YELLOW=$(tput setaf 3)
RED=$(tput setaf 1)
function red() {
    echo -e "$RED$*$NORMAL"
}

function green() {
    echo -e "$GREEN$*$NORMAL"
}

function yellow() {
    echo -e "$YELLOW$*$NORMAL"
}



function config_copy_files()
{
    yellow "install ..."
    local install_dir
    local install_dir

    source_dir="$1"
    install_dir="$2"

    echo "install log at "$(date)                   >  install.log
    echo "the following files have been installed:" >> install.log
    echo "-----AUTOMATICALLY GENERATED. DO NOT MODIFY------" >> install.log

    for f in $( ls -1  "$source_dir"  | grep -v setup.sh | grep -v install.log | grep -v installdir )
    do
        msg="copy $f to: \t $install_dir"

        echo "$install_dir/"$f   >> install.log

        red  $msg
        cp -i "$f" "$install_dir"
    done

}

function config_remove_files()
{
    yellow "uninstall ..."

    local install_dir
    local install_dir

    source_dir="$1"
    install_dir="$2"


    [[ ! -f install.log ]] && yellow "install.log not found! abort uninstalling!" && exit -3

    for f in $( awk 'NR >=4' install.log )
    do
        green "remove $f"

        if [[ -f  "$f" ]]
        then
            rm  "$f"
        else
            yellow "$f not found. so not removed."
        fi
    done
        rm install.log
}



[[ -z $1 ]] && echo "$( pwd -P ): what do you want to do? 'install' or 'uninstall'?" && exit -1;
[[ $( uname ) != 'Darwin' ]] && echo "$( pwd -P ): your OS doesn't look like OSX. abort!" && exit -2;


