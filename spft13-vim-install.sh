#!/bin/bash
#
# http://vim.spf13.com/
#

if [[ ! -f ~/.vimrc ]]; then
    # vimrc is not installed
    curl http://j.mp/spf13-vim3 -L -o - | sh
else
    echo "Do you want to update spft13-vim"

    cd $HOME/.spf13-vim-3/
    git pull
    mvim +BundleInstall! +BundleClean +q
fi



