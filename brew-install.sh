#!/bin/bash

function is_mac()
{
  if [[ $OSTYPE == darwin* ]]
  then
    echo "OSX check: pass"
    return 0
  else
    echo "You are not running OSX."
    return 1
  fi
}


function has_homebrew()
{
   type brew &> /dev/null
}

function install_homebrew()
{
    echo "Ok. Install homebrew now"

    ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)" > /tmp/homebrew-install.log
}

! is_mac  &&  exit -1

if ! has_homebrew
  then
    echo "You need to install homebrew first."
    echo "Do you wish to install Homebrew? (type 1 or 2)"
    select yn in "Yes" "No"; do
    case $yn in
        Yes ) install_homebrew; break;;
        No ) exit;;
    esac
done

fi



# update homebrew
echo "Update homebrew formulas (it may take a few minutes)..."
brew update
brew doctor

# upgrade already installed homebrew packages
brew upgrade


#install my standard packages
brew tap homebrew/versions
brew tap homebrew/dupes


for f in         \
    ack \
    armadillo \
    cmake \
    cscope \
    gcc \
    gcc44 \
    git \
    gnu-sed \
    imagemagick \
    macvim \
    pandoc \
    pdftk \
    pv \
    python \
    python3 \
    rmtrash \
    subversion \
    "--HEAD tidy" \
    wget \
    xpdf \
    ffmpeg \
    coreutils \
    grc
do
   echo "installing: "$f
   brew install $f
done



